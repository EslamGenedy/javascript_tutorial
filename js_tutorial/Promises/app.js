// setTimeout(function()
// {
//         console.log("ja");
// }
// ,50000)

// function sum ( a ,  b )
// {
//     if(b)
//     {
//         return a+b ; 

//     }
//     else{
//         return function dummy(c){
//             return a+c ; 
//         }
//     }
// }

// sum(1)(2); 
// //curring
// console.log(sum(1)(2));
// console.log(sum(1,5));

/* Calling Back */
// function summ (a , b , cb)
// {
//     setTimeout(()=>{
//       cb(a,b);
//     },3000);
// }

// summ (1,2 ,function(data1,data2)
// {
//     console.log(data1,data2 );
// })
//============================================================================//
/* IMAGE TAG THAT MAKING ERROR */
function createImage(url)
{
   let img = document.createElement('img') ; 
   img.src = url ; 
   img.width = 150  ; 
   img.height = 150 ;  
   document.body.appendChild(img);
   return img ; 
}
// console.log(createImage("download.jpg"));

var doc = { 
    'img':{
        elms:[
            {
                class:'logo',
                width:null , 
                height:null
            }
        ]
    }
}

// loadImage("download.jpg" , function(err,img){
//     if(err)
//     {
//         throw err ; 
//     }
//         createImage(img) ;
//         loadImage("2.jpg" , function(err,img){
//             if(err)
//             {
//                 throw err ; 
//             }
//                 createImage(img) ;
//                 loadImage("2.jpg" , function(err,img){
//                     if(err)
//                     {
//                         throw err ; 
//                     }
//                         createImage(img) ;
                        
//                 })
//         })
// })

//  loadImagePromised("download.jpg").then(img=>{
//     createImage(img) ;
//     loadImagePromised("2.jpg").then(img=>{
//         createImage(img) ;
//         loadImagePromised("2.jpg").then(img=>{
//             createImage(img) ;
//          }).catch(err=>{
//              throw err
//          })
//      }).catch(err=>{
//          throw err
//      })
//  }).catch(err=>{
//      throw err
//  })

/* Chain of Promises */

// loadImagePromised("download.jpg").then(img=>{
//         createImage(img) ;
//         return loadImagePromised("2.jpg") ; 
//      }).then(img => {
//          createImage(img);
//      }).catch(err=>{
//          throw err
//      })

/* Promise All */
Promise.all([
    loadImagePromised('download.jpg'),
    loadImagePromised('2.jpg'),
    loadImagePromised('2.jpg')
]).then(result =>{
    result.forEach(img =>{
        createImage(img);
    });
}).catch(err =>{
    throw err ; 
})
    

//============================================================================//
