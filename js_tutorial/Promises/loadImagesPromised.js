function loadImagePromised (url)
{
    return new Promise(function(resolve , reject){
        let image = new Image();
    image.onload =function()
    {
        resolve(image.src);
    }
    image.onerror = function()
    {
        reject(new Error('image is not founded in url :' + url))
    }
    image.src = url ;
    })
    
} 