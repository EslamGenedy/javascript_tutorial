function loadImage (url ,cb )
{
    let image = new Image();
    image.onload =function()
    {
        cb(null , image.src);
    }
    image.onerror = function()
    {
        cb(new Error('image is not founded in url :' + url))
    }
    image.src = url ;
} 